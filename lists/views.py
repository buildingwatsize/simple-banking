from django.shortcuts import redirect, render
from lists.models import Item

def home_page(request):

    
    if request.method == 'POST':
        amount = float(request.POST.get('item_text',"0"))
        for i in Item.objects.all():
            amount = amount+float(i.text)

        Item.objects.create(text=amount)
        return redirect('/')

    items = Item.objects.all()
    return render(request, 'home.html', {'items': items})
